#!/bin/bash

#SBATCH --job-name=touch-scratch
#SBATCH --partition=amarsden
#SBATCH --output=touch-scratch-%j.log
#SBATCH --error=touch-scratch-%j.log
#SBATCH --time=1:00:00
#SBATCH --qos=normal
#SBATCH --nodes=1

find /scratch/users/$USER/ -type f -exec touch {} +