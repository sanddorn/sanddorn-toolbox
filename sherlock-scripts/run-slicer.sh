#!/bin/bash

#SBATCH --job-name=slicer
#SBATCH --partition=amarsden
#SBATCH --output=slicer_out_%j
#SBATCH --error=slicer_out_%j
#SBATCH --time=1:00:00
#SBATCH --qos=normal
#SBATCH --nodes=1
#SBATCH --mem=32GB
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=24

# Load modules
module purge
module load system
module load binutils/2.38
module load qt
module load openmpi
module load mesa
module load cmake/3.23.1
module load gcc/12.1.0

OMP_NUM_THREADS=24 /home/users/richter7/sanddorn-toolbox/slicer/Release/slicer \
    path/to/threed_results.vtu \
    path/to/centerline.vtp \
    path/to/centerline_results.vtp
