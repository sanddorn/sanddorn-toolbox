#!/bin/bash

#SBATCH --job-name=estimator
#SBATCH --partition=amarsden
#SBATCH --output=estimator-%j.log
#SBATCH --error=estimator-%j.log
#SBATCH --time=48:00:00
#SBATCH --qos=normal
#SBATCH --nodes=2
#SBATCH --mem=32GB
#SBATCH --ntasks-per-node=24

# Load Modules
module purge
module load system
module load binutils/2.38
module load qt
module load openmpi
module load mesa
module load cmake/3.23.1
module load gcc/12.1.0
module load x11
module load sqlite/3.37.2

# Command
~/miniconda3/envs/estimator/bin/python ~/svSuperEstimator/main.py ~/svSuperEstimator/config.yaml
