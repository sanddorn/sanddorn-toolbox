#!/bin/bash

#SBATCH --job-name=build-svsolver
#SBATCH --partition=amarsden
#SBATCH --output=build-svsolver-%j.log
#SBATCH --error=build-svsolver-%j.log
#SBATCH --time=1:00:00
#SBATCH --qos=normal
#SBATCH --nodes=1
#SBATCH --mem=16GB
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4

# Load modules
module --force purge
module load devel
module load math
module load openmpi
module load openblas
module load system
module load x11
module load mesa
module load gcc/8.1.0
module load cmake
ml labs
ml load poldrack

# Start script
if [ ! -d "./svsolver/src" ]; then
   echo "Creating source directory ./svsolver/src"
   mkdir -p ./svsolver/src
   echo "Cloning svsolver to ./svsolver/src"
   git clone --recursive https://github.com/SimVascular/svSolver.git ./svsolver/src
else
   echo "Updating svsolver source"
   (cd ./svsolver/svsolver; git pull)
fi

if [ ! -d "./svsolver/build" ]; then
   echo "Creating build directory ./svsolver/build"
   mkdir -p ./svsolver/build
fi

echo "Start building"
export CC=/share/software/user/open/gcc/8.1.0/bin/gcc
export CXX=/share/software/user/open/gcc/8.1.0/bin/g++
cmake \
   -DCMAKE_BUILD_TYPE=Release \
   -DCMAKE_CXX_COMPILER=/share/software/user/open/gcc/8.1.0/bin/g++ \
   -DCMAKE_C_COMPILER=/share/software/user/open/gcc/8.1.0/bin/gcc \
   -S ./svsolver/src -B ./svsolver/build/

cmake --build ./svsolver/build/ -- -j4
