#!/bin/bash

#SBATCH --job-name=build-vtk
#SBATCH --partition=amarsden
#SBATCH --output=build-vtk-%j.log
#SBATCH --error=build-vtk-%j.log
#SBATCH --time=1:00:00
#SBATCH --qos=normal
#SBATCH --nodes=1
#SBATCH --mem=16GB
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4

# Load modules
module purge
module load system
module load binutils/2.38
module load qt
module load openmpi
module load mesa
module load cmake/3.23.1
module load gcc/12.1.0

# Start script
if [ ! -d "./vtk/src" ]; then
   echo "Creating source directory ./vtk/src"
   mkdir -p ./vtk/src
   echo "Cloning vtk to ./vtk/src"
   git clone --recursive https://gitlab.kitware.com/vtk/vtk.git ./vtk/src
else
   echo "Found vtk source"
   (cd ./vtk/src; git pull)
fi

if [ ! -d "./vtk/build" ]; then
   echo "Creating build directory ./vtk/build"
   mkdir -p ./vtk/build
fi

echo "Start building"
cmake -DBUILD_SHARED_LIBS:BOOL=ON \
   -DCMAKE_BUILD_TYPE=Release \
   -DCMAKE_CXX_COMPILER=/share/software/user/open/gcc/12.1.0/bin/g++ \
   -DCMAKE_C_COMPILER=/share/software/user/open/gcc/12.1.0/bin/gcc \
   -S ./vtk/src -B ./vtk/build/

cmake --build ./vtk/build/ -- -j4

rm -rf ./vtk/src
