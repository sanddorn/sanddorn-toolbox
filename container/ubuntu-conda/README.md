# Ubuntu image with miniconda

## Build

```bash
docker build -t sanddorn-ubuntu-conda .
```

## Download

```bash
docker pull registry.gitlab.com/sanddorn/sanddorn-toolbox/sanddorn-ubuntu-conda
```

## Run

```bash
docker run -it sanddorn-ubuntu-conda
```
